# Soler SDK

## Overview

The Soler SDK is a high-level, dual-stack [Grid](https://gitlab.com/thegridprotocol/home#the-grid)/Matrix Java SDK.

It provides a unified set of classes and interfaces, letting your application connect to both network seamlessly.

## Status

Currently in the works for a first release. Keep in touch for news about our progress!

## Contact

On Grid:

- *Not read yet, we are bootstrapping!*

On Matrix:

- #soler:kamax.io
