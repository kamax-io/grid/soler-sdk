/*
 * Copyright (C) 2019 Kamax Sarl
 *
 * https://www.kamax.io/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.kamax.grid.soler.core;

import com.google.gson.JsonObject;
import io.kamax.grid.soler.api.GsonUtil;
import io.kamax.grid.soler.api.SolerIdentityClient;
import io.kamax.grid.soler.core.io.UIAuthJson;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Objects;

public class SolerIdentityHttpClient implements SolerIdentityClient {

    private static final Logger log = LoggerFactory.getLogger(SolerIdentityHttpClient.class);

    private OkHttpClient client;
    private String endpoint;
    private String accessToken;

    public SolerIdentityHttpClient(OkHttpClient client, String endpoint) {
        this.client = client;
        this.endpoint = endpoint;
    }

    @Override
    public UIAuthJson auth(JsonObject stepData) {
        RequestBody reqBody = RequestBody.create(MediaType.get("application/json"), GsonUtil.toJson(stepData));
        Request req = new Request.Builder().post(reqBody).url(endpoint + "/identity/client/v0/do/auth").build();
        try (Response res = client.newCall(req).execute()) {
            String body = Objects.requireNonNull(res.body()).string();
            if (res.code() == 401) {
                UIAuthJson o = GsonUtil.parse(body, UIAuthJson.class);
                if (o.hasError()) {
                    throw new SecurityException(o.getError().getMessage());
                }

                return o;
            }

            if (res.code() == 200) {
                UIAuthJson o = GsonUtil.parse(body, UIAuthJson.class);
                o.setComplete(true);
                return o;
            } else {
                // FIXME
                throw new RuntimeException();
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public UIAuthJson initLogin() {
        Request req = new Request.Builder().get().url(endpoint + "/identity/client/v0/do/login").build();
        try (Response res = client.newCall(req).execute()) {
            String body = Objects.requireNonNull(res.body()).string();
            if (res.code() != 200) {
                // FIXME
                throw new RuntimeException();
            }

            return GsonUtil.parse(body, UIAuthJson.class);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public String login(String sessionId) {
        RequestBody reqBody = RequestBody.create(MediaType.get("application/json"), GsonUtil.toJson(GsonUtil.makeObj("session", sessionId)));
        Request req = new Request.Builder().post(reqBody).url(endpoint + "/identity/client/v0/do/login").build();
        try (Response res = client.newCall(req).execute()) {
            String bodyRaw = Objects.requireNonNull(res.body()).string();
            if (res.code() != 200) {
                // FIXME
                throw new RuntimeException();
            }

            JsonObject body = GsonUtil.parseObj(bodyRaw);
            String token = GsonUtil.getStringOrThrow(body, "token"); // FIXME throw a non-compliant server exception
            accessToken = token;
            return token;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public String getEndpoint() {
        return endpoint;
    }

    @Override
    public String getAccessToken() {
        return accessToken;
    }

}
