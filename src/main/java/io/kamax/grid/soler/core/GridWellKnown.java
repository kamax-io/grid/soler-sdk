/*
 * Copyright (C) 2019 Kamax Sarl
 *
 * https://www.kamax.io/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.kamax.grid.soler.core;

import com.google.gson.annotations.SerializedName;

public class GridWellKnown {

    public static class Role {

        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }

    @SerializedName("v")
    private String version;
    @SerializedName("g.identity")
    private Role identity = new Role();
    @SerializedName("g.data")
    private Role data = new Role();

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Role getIdentity() {
        return identity;
    }

    public void setIdentity(Role identity) {
        this.identity = identity;
    }

    public Role getData() {
        return data;
    }

    public void setData(Role data) {
        this.data = data;
    }

}
