/*
 * Copyright (C) 2019 Kamax Sarl
 *
 * https://www.kamax.io/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.kamax.grid.soler.core;

import io.kamax.grid.soler.api.GsonUtil;
import io.kamax.grid.soler.api.SolerClient;
import io.kamax.grid.soler.api.SolerEndpoint;
import io.kamax.grid.soler.api.SolerIdentityClient;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.util.Objects;
import java.util.Optional;

public class SolerHttpClient implements SolerClient {

    private static final Logger log = LoggerFactory.getLogger(SolerHttpClient.class);

    private OkHttpClient client;

    public SolerHttpClient() {
        client = new OkHttpClient.Builder().build();
    }

    @Override
    public Optional<SolerEndpoint> discover(String domain) {
        SolerHttpEndpoint endpoint = new SolerHttpEndpoint();
        Request req = new Request.Builder().get().url("https://" + domain + "/.well-known/grid").build();
        try (Response res = client.newCall(req).execute()) {
            if (res.code() == 404) {
                return Optional.empty();
            }

            String body = Objects.requireNonNull(res.body()).string();
            if (res.code() != 200) {
                // FIXME
                throw new RuntimeException();
            }

            log.info("Found Well-known");

            GridWellKnown discovery = GsonUtil.parse(body, GridWellKnown.class);
            log.info("Well-known is a JSON object");
            log.info("Version: {}", discovery.getVersion());
            if (!StringUtils.equals("0.0",discovery.getVersion())) {
                log.warn("Well-known version is not supported: {}", discovery.getVersion());
                return Optional.empty();
            }

            log.info("Version is supported");
            if (StringUtils.isNotEmpty(discovery.getIdentity().getUrl())) {
                log.info("Found a value for Identity URL");
                endpoint.setIdentity(URI.create(discovery.getIdentity().getUrl()));
            } else {
                log.info("No value for Identity URL");
            }

            // TODO handle Data server

            return Optional.of(endpoint);
        } catch (IOException exIo) {
            throw new UncheckedIOException(exIo);
        }
    }

    @Override
    public SolerIdentityClient createIdentity(SolerEndpoint endpoint) {
        return new SolerIdentityHttpClient(client, endpoint.getIdentity().toString());
    }

}
